#include <iostream>
 // krawedzie = (int*) malloc(sizeof(int) * (_rows+1) *(_cols+1));
#include <cstdlib>
#include <string.h>
#include "Board.h"

#define IND(i, j, n) ((i)*(n)+(j))

#define DOWN_E 1
#define LEFT_E 2
#define SLSH_E 4
#define BSLSH_E 8
#define ALL 15

Board::Board(const int rows, const int cols)
{
  _rows = rows+4;
  _cols = cols+2;

  pos_x = (rows)/2+2; // 10+1 pozycji + 2 na bramki, ale bierzemy srodek
  pos_y = cols/2;     // 8+1 pozycji, ale bierzemy srodek

  edges = new char [_rows * _cols];
  int toGate = cols/2 -1;

  // wypelnij wszystko 0
  for(int i = 0; i < _rows; i++) {
    for(int j = 0; j < _cols; j++) {
      edges[IND(i,j,_cols)] = 0;
    }
  }

  // przy bramkach
  for(int i = 0; i < toGate; i++) {
    edges[IND(1, i+1 ,_cols)] = ALL;
    edges[IND(1, toGate+i+3 ,_cols)] = ALL;

    edges[IND(_rows -2 , i+1 ,_cols)] = ALL;
    edges[IND(_rows -3 , i+1 ,_cols)] = DOWN_E;

    edges[IND(_rows -2, toGate+i+3 ,_cols)] = ALL;
    edges[IND(_rows -3, toGate+i+3 ,_cols)] = DOWN_E;
  }
  // lewa krawedz w bramce
  edges[IND(1, toGate+1 ,_cols)] += LEFT_E;
  edges[IND(_rows -2, toGate+1 ,_cols)] += LEFT_E;

  // dolna krawedz w dolnej bramce
  edges[IND(_rows -2, toGate+1 ,_cols)] += DOWN_E;
  edges[IND(_rows -2, toGate+2 ,_cols)] = DOWN_E;

  //pionowe
  for(int i = 0; i < _rows; i++) {
    edges[IND(i, 0 ,_cols)] = ALL;
    edges[IND(i, 1 ,_cols)] |= LEFT_E;
    edges[IND(i, _cols-1 ,_cols)] = ALL;
  }

  //poziome
  for(int i = 0; i < _cols; i++) {
    edges[IND(0, i+1 ,_cols)] = ALL;
    edges[IND(_rows-1, i+1 ,_cols)] = ALL;
  }
}

void Board::copy(const Board *p) {
  for(int i=0; i < p->_rows * p->_cols; ++i) 
    edges[i] = p->edges[i];

  _rows = p->_rows;
  _cols = p->_cols;
  pos_x = p->pos_x;
  pos_y = p->pos_y;
}

Board::Board(const Board* p) {
  edges = new char [p->_rows * p->_cols];
  for(int i = 0; i < p->_rows * p->_cols; ++i) {
    edges[i]  = p->edges[i];
  }

  _rows = p->_rows;
  _cols = p->_cols;
  pos_x = p->pos_x;
  pos_y = p->pos_y;

}

Board::~Board()
{
  delete(edges);
  edges = NULL;
}

void Board::print() const {
  std::string sides, downs;
  int tempEdge;

  for(int i = 0; i < _rows; i++) {
    sides = std::string();
    downs = std::string();
    for(int j = 0; j < _cols; j++){
      tempEdge = edges[IND(i,j,_cols)];

      //pozycja pilki
      if(pos_x-1 == i && pos_y+1 == j)
        downs.append("*");
      else
        downs.append(" ");

      //kreski poziome
      if(tempEdge & DOWN_E) {
        downs.append("-");
      }
      else {
        downs.append(" ");
      }

      //kreski piomnowe i ukosne
      if(tempEdge & LEFT_E)
        sides.append("|");
      else
        sides.append(" ");

      if(tempEdge & SLSH_E) {
        if(tempEdge & BSLSH_E) {
          sides.append("x");
        }
        else {
          sides.append("/");
        }
      }
      else {
        if(tempEdge & BSLSH_E) {
          sides.append("\\");
        }
        else {
          sides.append("o");
        }
      }
    }
    std::cerr << sides << std::endl;
    std::cerr << downs << std::endl;
  }
}

bool Board::isValid(int dir) const {
  switch (dir) {
    case 0: // prawo
      if(edges[IND(pos_x-1, pos_y+1,_cols)] & DOWN_E )
        return false;
      break;

    case 1: // prawy gorny
      if(edges[IND(pos_x-1, pos_y+1,_cols)] & SLSH_E )
        return false;
      break;

    case 2: // gora
      if(edges[IND(pos_x-1, pos_y+1,_cols)] & LEFT_E )
        return false;
      break;

    case 3: // lewy gorny
      if(edges[IND(pos_x-1, pos_y,_cols)] & BSLSH_E )
        return false;
      break;

    case 4: // lewo
      if(edges[IND(pos_x-1, pos_y,_cols)] & DOWN_E )
        return false;
      break;

    case 5: // lewy dol
      if(edges[IND(pos_x, pos_y,_cols)] & SLSH_E )
        return false;
      break;

    case 6: // dol
      if(edges[IND(pos_x, pos_y+1,_cols)] & LEFT_E )
        return false;
      break;

    case 7: // prawy dol
      if(edges[IND(pos_x, pos_y+1,_cols)] & BSLSH_E )
        return false;
      break;
  }
  return true;

}

int Board::playMove(int dir) {
  switch (dir) {
    case 0: // prawo
      if(edges[IND(pos_x-1, pos_y+1,_cols)] & DOWN_E )
        return 1;

      edges[IND(pos_x-1, pos_y+1,_cols)] |= DOWN_E;
      ++pos_y;
      break;

    case 1: // prawy gorny
      if(edges[IND(pos_x-1, pos_y+1,_cols)] & SLSH_E )
        return 1;
      edges[IND(pos_x-1, pos_y+1,_cols)] |= SLSH_E;
       ++pos_y;
       --pos_x;
      break;

    case 2: // gora
      if(edges[IND(pos_x-1, pos_y+1,_cols)] & LEFT_E )
        return 1;
      edges[IND(pos_x-1, pos_y+1,_cols)] |= LEFT_E;
      --pos_x;
      break;

    case 3: // lewy gorny
      if(edges[IND(pos_x-1, pos_y,_cols)] & BSLSH_E )
        return 1;
      edges[IND(pos_x-1, pos_y,_cols)] |= BSLSH_E;
      --pos_x;
      --pos_y;
      break;

    case 4: // lewo
      if(edges[IND(pos_x-1, pos_y,_cols)] & DOWN_E )
        return 1;
      edges[IND(pos_x-1, pos_y,_cols)] |= DOWN_E;
      --pos_y;
      break;

    case 5: // lewy dol
      if(edges[IND(pos_x, pos_y,_cols)] & SLSH_E )
        return 1;
      edges[IND(pos_x, pos_y,_cols)] |= SLSH_E;
      ++pos_x;
      --pos_y;
      break;

    case 6: // dol
      if(edges[IND(pos_x, pos_y+1,_cols)] & LEFT_E )
        return 1;
      edges[IND(pos_x, pos_y+1,_cols)] |= LEFT_E;
      ++pos_x;
      break;

    case 7: // prawy dol
      if(edges[IND(pos_x, pos_y+1,_cols)] & BSLSH_E )
        return 1;
      edges[IND(pos_x, pos_y+1,_cols)] |= BSLSH_E;
      ++pos_x;
      ++pos_y;
      break;
      
  
  }
  
  return 0;
}

std::array<double, 8> Board::validMovesProb(const double sideProb, const double upProb,
                                            const double dolP) const {
  std::array<double, 8> res;

  res[0] = (edges[IND(pos_x-1, pos_y+1,_cols)] & DOWN_E ) ? 0.0 : sideProb;
  res[1] = (edges[IND(pos_x-1, pos_y+1,_cols)] & SLSH_E) ? 0.0 : upProb;
  res[2] = (edges[IND(pos_x-1, pos_y+1,_cols)] & LEFT_E  ) ? 0.0 : upProb;
  res[3] = (edges[IND(pos_x-1, pos_y,_cols)]   & BSLSH_E) ? 0.0 : upProb;
  res[4] = (edges[IND(pos_x-1, pos_y,_cols)]   & DOWN_E ) ? 0.0 : sideProb;
  res[5] = (edges[IND(pos_x, pos_y,_cols)]     & SLSH_E) ? 0.0 : dolP;
  res[6] = (edges[IND(pos_x, pos_y+1,_cols)]   & LEFT_E  ) ? 0.0 : dolP;
  res[7] = (edges[IND(pos_x, pos_y+1,_cols)]   & BSLSH_E) ? 0.0 : dolP;

  return res;
}

bool Board::isEnd() const {
  return !(isValid(0) || isValid(1) || isValid(2) ||
           isValid(3) || isValid(4) || isValid(5) ||
           isValid(6) || isValid(7)
          ) || (pos_y == 4 && (pos_x == 1 || pos_x == 13)); 

}

bool Board::isNewField() const {
  int res = 0; 

  if(edges[IND(pos_x-1, pos_y+1,_cols)] & DOWN_E )
    ++res;

  if(edges[IND(pos_x-1, pos_y+1,_cols)] & SLSH_E )
    ++res;

  if(edges[IND(pos_x-1, pos_y+1,_cols)] & LEFT_E )
    ++res;

  if(edges[IND(pos_x-1, pos_y,_cols)] & BSLSH_E )
    ++res;

  if(edges[IND(pos_x-1, pos_y,_cols)] & DOWN_E )
    ++res;

  if(edges[IND(pos_x, pos_y,_cols)] & SLSH_E )
    ++res;

  if(edges[IND(pos_x, pos_y+1,_cols)] & LEFT_E )
    ++res;

  if(edges[IND(pos_x, pos_y+1,_cols)] & BSLSH_E )
    ++res;

  return (res<=1);
}

bool Board::isNewField(int dir) const {
  int res = 0; 
  int px = pos_x;
  int py = pos_y;
  switch(dir) {
    case 0:
      py++; break;
    case 1:
      py++; px--; break;
    case 2:
      px--; break;
    case 3:
      px--; py--; break;
    case 4:
      py--; break;
    case 5:
      px++; py--; break;
    case 6:
      px++; break;
    case 7:
      px++; py++; break;
  }

  if(edges[IND(px-1, py+1,_cols)] & DOWN_E )
    ++res;

  if(edges[IND(px-1, py+1,_cols)] & SLSH_E )
    ++res;

  if(edges[IND(px-1, py+1,_cols)] & LEFT_E )
    ++res;

  if(edges[IND(px-1, py,_cols)] & BSLSH_E )
    ++res;

  if(edges[IND(px-1, py,_cols)] & DOWN_E )
    ++res;

  if(edges[IND(px, py,_cols)] & SLSH_E )
    ++res;

  if(edges[IND(px, py+1,_cols)] & LEFT_E )
    ++res;

  if(edges[IND(px, py+1,_cols)] & BSLSH_E )
    ++res;

  return (res==0);
}

std::vector<int> Board::validMoves() const {
  std::vector<int> wyn;

  for(int i=0; i < 8; i++)
    if(isValid(i))
      wyn.push_back(i);

  return wyn;
}

int Board::amIWinner(const bool czyMojRuch) const {
  //albo pilka w bramce 
  if(pos_y == 4 && pos_x == 13) 
      return 0;
  else if(pos_y == 4 && pos_x == 1) 
      return 1;
  
  else if(czyMojRuch) 
      return 0; //zostalismy zablokowani
  else 
      return 1;
}

