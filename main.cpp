#include <iostream>
#include "Board.h"
#include "MCTS.h"
#include <string.h>
#include <cstdio>

using namespace std;

int main() {
  char command[256];
  int rows = 10,
      cols = 8,
      move,
      leftTime;

    scanf("boardsize %d %d", &cols, &rows);
    std::cout << "="<< std::endl << std::endl;
    fflush(stdout);

  MCTS g(rows, cols);

  while(!g.isEnd() ) {
      scanf("%s", command);
      if(strcmp(command, "play") ==0 ){
        std::cin >> move;
        g.playMove(move);
        std::cout << "="<< std::endl << std::endl;
        fflush(stdout);
      }
      else if(strcmp(command, "timeleft") ==0 ){
        std::cin >> leftTime;
        std::cout << "="<< std::endl << std::endl;
        fflush(stdout);
      }
      else if(strcmp(command, "genmove") ==0 ){
        move = g.genMove();
        std::cout <<"= " << move << std::endl << std::endl;
        fflush(stdout);
      }
      else {
        std::cout <<"IO ERROR" << std::endl << std::endl; //TODO
      }

    }


    fflush(stdout);

    return 0;
}
