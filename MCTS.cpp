#include "MCTS.h"
#include <cstdlib>
#include <time.h>

#include <iostream>
#include <chrono>
#include <random>
#include <functional>
#include <array>

#define STAT
#define PRNT
#define PROG 2 

MCTS::MCTS(const int rows, const int cols) {
  b = new Board(rows, cols);
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  generator = std::default_random_engine(seed);
  tree = new Node[MAX_NODES];
}

MCTS::~MCTS() {
  delete b;
}

void MCTS::printBoard() {
#ifdef PRNT
  b->print();
#endif
}

int MCTS::playMove(int gdzie) {
  return b->playMove(gdzie);
}

int MCTS::genMove() {
  bool isMyTurn = true;
  bool isEnd;
  int bestMove = -1;;
  int idx2;

  std::array<double,8> movsProb;
  std::discrete_distribution<int> distr;

  std::vector<int> vm = b->validMoves();
  if (vm.size() == 1) {
    b->playMove(vm[0]);
    return vm[0];
  }

  idx = 1; //pierwszy pusty
  //inicjujemy i rozwijamy root
  tree[0].initNode(-1, -1, true);
  extend(0, b);
  
  bestMove = closeMove(true, *b);
  if(bestMove != -1) {
      b->playMove(bestMove);
      return bestMove;
  }    
  Board tp(b);
  for(long long it = 0; it < getRepeatNum(); it++) {
    tp.copy(b);
    isMyTurn = true;
    idx2 = 0;
 
    // konczymu szukanie gdy- jestesmy lisciem lub nie ma ruchu
    isEnd = false;
    while(!isEnd) {
      idx2 = bestChildUCB(idx2);

      tp.playMove(tree[idx2]._move);
      if (tp.isNewField() ) {
        isMyTurn = !isMyTurn;
      }
      // albo nie ma ruchu albo lisc
      isEnd = tp.isEnd() || !tree[idx2].isExt;
    }
    if((MAX_NODES - 8 > idx) && (!tree[idx2].isExt) 
        && (tree[idx2]._n  > PROG) && (!tp.isEnd() )) {
      extend(idx2, &tp);
    }
      //losowe rozgrywki
    isEnd = tp.isEnd();
    while(!isEnd) {
      bestMove = closeMove(isMyTurn, tp);
      if(bestMove == -1) {
        if(isMyTurn)
          movsProb = tp.validMovesProb(getSideProb(), getUpProb(), getDownProb());
        else
          movsProb = tp.validMovesProb(getSideProb(), getDownProb(), getUpProb());
        distr = std::discrete_distribution<int>(movsProb.begin(),movsProb.end());
        bestMove = distr(generator);
      }

      tp.playMove(bestMove);
      if (tp.isNewField() ) {
        isMyTurn = !isMyTurn;
      }
      isEnd = tp.isEnd();
    }

      //aktualizacja wynikow
    int skl = tp.amIWinner(isMyTurn);

    while(idx2 != -1) {
      tree[idx2]._w += skl;
      tree[idx2]._n += 1;
      idx2 = tree[idx2]._parent;
    }
  }
  bestMove = bestRootChild();
  b->playMove(bestMove);
  return bestMove;
}

void MCTS::extend(const int node, Board *b) {
  bool timm;
  std::vector<int> dR = b->validMoves();
  tree[node]._fchild = idx;
  tree[node]._childNum = dR.size();
  tree[node].isExt = true;

  for(int ruch: dR) {
    timm = b->isNewField(ruch)? !tree[node].isMyMove : tree[node].isMyMove;
    tree[idx].initNode(node, ruch, timm);
    idx++;
  }
}

void Node::initNode(const int parent, const int ruch, bool imm) {
  _w = 0;
  _n = 0;
  _parent = parent;
  isExt = false;
  _move = ruch;
  isMyMove = imm;
}

double MCTS::UCB(const int node, double c) const {
  int parent = tree[node]._parent;
  double wyn = 10000000000.0;

  if (tree[node]._n != 0 && tree[parent]._n != 0) {
    if(tree[parent].isMyMove) {
      wyn = (1.0 * tree[node]._w)/tree[node]._n + c * sqrt(log((1.0 * tree[parent]._n))/ tree[node]._n);
    } 
    else {
     wyn = -(1.0 * tree[node]._w)/tree[node]._n + c * sqrt(log((1.0 * tree[parent]._n))/ tree[node]._n);
    }
  }
  return wyn;
}

int MCTS::bestChildUCB(const int node){
  double maxSyn = -std::numeric_limits<double>::max();
  double tempUCB;
  int naj = -1;
  std::uniform_real_distribution<double> noise(0, 0.001);

  int maxChild = tree[node]._fchild + tree[node]._childNum;
  for(int k = tree[node]._fchild; k < maxChild; ++k) {
    tempUCB = UCB(k, 1.5)+noise(generator);
    if(maxSyn <= tempUCB) {
      maxSyn = tempUCB;
      naj = k;
    }
  }
  return naj;
}

int MCTS::bestRootChild() const {
  double maxN = -1;
  int naj = -1;

  if (tree[0]._childNum <= 0) {
    std::cerr << "Error nie ma najlepszego syna dla "
              << std::endl;
    return -1;
  }

  int maxChild = tree[0]._fchild + tree[0]._childNum;
  for(int k = tree[0]._fchild; k < maxChild; ++k) {
    if(maxN < tree[k]._n) {
      maxN = tree[k]._n;
      naj = tree[k]._move;
    }
  }
  return naj;
}

double MCTS::moveProb(const int kier) const {
  double sum = getDownProb() + getUpProb() + getSideProb();
  if(kier >= 1 && kier <= 3)
    return getUpProb()/sum;
  if(kier >=5 )
    return getDownProb()/sum;
  return getSideProb()/sum;
}

int MCTS::closeMove(const bool isMyTurn, Board &p) const {
    if(isMyTurn) {
        if(p.pos_x > 3) {
          return -1;
        }
        else if( p.pos_x == 3) {
          if (p.pos_y == 2 && p.isValid(1)) return 1;
          else if (p.pos_y == 3 && p.isValid(2)) return 2;
          else if (p.pos_y == 4 && p.isValid(1)) return 1;
          else if (p.pos_y == 4 && p.isValid(3)) return 3;
          else if (p.pos_y == 5 && p.isValid(2)) return 2;
          else if (p.pos_y == 6 && p.isValid(3)) return 3;
          else return -1;
        }
        else if( p.pos_x == 2 ) { //linia bramkowa
          if (p.pos_y == 3 && p.isValid(1)) return 1;
          else if (p.pos_y == 4 && p.isValid(2)) return 2;
          else if (p.pos_y == 5 && p.isValid(3)) return 3;
          else return -1;
        }
        else if( p.pos_x == 1) {
          return -1; // blad
        } 
    }
    else {
        if(p.pos_x < 11) {
          return -1;
        }
        else if( p.pos_x == 11) {
          if (p.pos_y == 2 && p.isValid(7)) return 7;
          else if (p.pos_y == 3 && p.isValid(6)) return 6;
          else if (p.pos_y == 4 && p.isValid(5)) return 5;
          else if (p.pos_y == 4 && p.isValid(7)) return 7;
          else if (p.pos_y == 5 && p.isValid(6)) return 6;
          else if (p.pos_y == 6 && p.isValid(5)) return 5;
          else return -1;
        }
        else if( p.pos_x == 12 ) { //linia bramkowa
          if (p.pos_y == 3 && p.isValid(7)) return 7;
          else if (p.pos_y == 4 && p.isValid(6)) return 6;
          else if (p.pos_y == 5 && p.isValid(5)) return 5;
          else return -1;
        }
        else if( p.pos_x == 13) {
          return -1; // blad
        } 
        
    }
}

