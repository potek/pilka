#ifndef MCTS_H
#define MCTS_H
#include "Board.h"

#include <chrono>
#include <random>

#define MAX_NODES 2000000

class Node {
  public:
    int _w,
        _n,
        _parent,
        _fchild,
        _childNum,
        _move;
    bool isMyMove;
    bool isExt; //czy rozwiniety

    void initNode(const int parent, const int move, bool imm);
};


class MCTS
{
  public:
    Board *b;
    int idx; // pierwsze wolne pole w drzewie
    Node *tree;

    std::default_random_engine generator;

    MCTS(const int rows, const int cols);
    virtual ~MCTS();

    void printBoard();

    int playMove(int gdzie);
    int genMove();

    double getUpProb() const { return 101.0; };
    double getDownProb() const { return 100.0; };
    double getSideProb() const { return 99.0; };

    double moveProb(const int where) const;
    
    long long getRepeatNum() const { return 1400000; };

    void extend(const int node, Board *b);
    double UCB(const int node, double c) const;

    int bestChildUCB(const int node);
    int bestRootChild() const;
    
    int closeMove(const bool isMyTurn, Board &p) const;

    bool isEnd() const {return b->isEnd();};

  protected:
  private:
};

#endif // MCTS_H
