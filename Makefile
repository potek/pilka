CC = g++
CCFLAGS = -g -std=c++11 -O3


all: ps306453 

clean:
	rm -f *.o

ps306453: Board.o MCTS.o main.o
	${CC} ${CCFLAGS} *.o -o ps306453 
        
Board.o: Board.cpp Board.h
	${CC} ${CCFLAGS} -c Board.cpp

MCTS.o: MCTS.cpp MCTS.h 
	${CC} ${CCFLAGS} -c MCTS.cpp

main.o: main.cpp
	${CC} ${CCFLAGS} -c main.cpp
