#ifndef PLANSZA_H
#define PLANSZA_H

#include<array>
#include<vector>

class Board
{
  public:
    Board(const int rows, const int cols);
    Board(const Board* p);

    virtual ~Board();

    char *edges;
    int _rows;
    int _cols;
    int pos_x;
    int pos_y;

    void print() const;
    void copy(const Board* p);

    int playMove(int gdzie);
    bool isValid(int gdzie) const;
    bool isEnd() const;
    bool isNewField() const;
    bool isNewField(int dir) const ;

    std::vector<int> validMoves() const;
    int amIWinner(const bool isMyMove) const;

    std::array<double, 8> validMovesProb(const double sideProb, const double upProb, const double downProb) const;

  protected:
  private:
};

#endif // PLANSZA_H
